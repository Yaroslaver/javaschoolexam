package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if (y == null || x == null) {
            throw new IllegalArgumentException();
        }

        if (!y.containsAll(x)) {
            return false;
        }

        List<Integer> indexesOfElems = new ArrayList<>();
        for (Object elem: x) {
            indexesOfElems.add(y.indexOf(elem));
        }

        for (int i = 0; i < indexesOfElems.size() - 1; ++i) {
            for (int j = i + 1; j < indexesOfElems.size(); ++j) {
                if (indexesOfElems.get(i) > indexesOfElems.get(j)) {
                    return false;
                }
            }
        }

        return true;
    }
}
