package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers == null) {
            throw new CannotBuildPyramidException();
        }

        if (inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException();
        }

        if (!inputNumbers.stream().allMatch(Objects::nonNull)) {
            throw new CannotBuildPyramidException();
        }

        int height = calculateHeightOfPyramid(inputNumbers);
        if (height == -1) {
            throw new CannotBuildPyramidException();
        }

        int width = 2 * height - 1;
        int[][] pyramid2D = new int[height][width];

        inputNumbers.sort(Comparator.naturalOrder());

        int offset = 0;
        int countElemInRow = 0;
        int currNumIndex = inputNumbers.size() - 1;
        for (int i = height - 1; i >= 0; --i) {
            countElemInRow = 0;
            for (int j = width - 1 - offset; j >= 0 && i > countElemInRow - 1; j -= 2) {
                pyramid2D[i][j] = inputNumbers.get(currNumIndex);
                --currNumIndex;
                ++countElemInRow;
            }
            ++offset;
        }

        return pyramid2D;
    }

    /**
     * Calculates height of the pyramid.
     *
     * @param numbers to be used in the pyramid
     * @return height of pyramid if the pyramid can be built, or -1 in other case
     */
    private int calculateHeightOfPyramid(List<Integer> numbers) {

        int listSize = numbers.size();

        int pyramidHeight = 0;
        while (listSize > 0) {
            ++pyramidHeight;
            listSize -= pyramidHeight;
        }

        return listSize == 0 ? pyramidHeight : -1;
    }

}
