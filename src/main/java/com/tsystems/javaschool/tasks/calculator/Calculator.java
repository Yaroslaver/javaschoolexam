package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");

        if (statement == null) {
            return null;
        }

        statement = statement.trim();

        // filter JS words
        if (!statement.matches("[^a-zA-Z,]+")) {
            return null;
        }

        for (int i = 0; i < statement.length() - 1; ++i) {
            if (statement.charAt(i) == '/' && statement.charAt(i + 1) == '/') {
                return null;
            }
        }

        try {
            Object result = engine.eval(statement);
            String strRes = String.valueOf(result);
            // filter Infinity, - Infinity and NaN
            if (strRes.matches("[-a-zA-Z]+")) {
                return null;
            }
            return strRes;
        }
        catch (ScriptException e) {
            return null;
        }

    }

}
